# Houdini Tips and Tricks

A repository of example files demonstrating different things that I've found to be useful - mostly written for me so I can remember things but hopefully will be helpful to you as well. 

Files
====

* FBO-like-setup : this is a set up that gets you some of the functionality of a Framebuffer object when programming with something like Web/OpenGL or other graphics apis. 
* copsample : This set up goes through the basics of setting up a scene to get rendered inside of a Composite network. 
* NullNodeAsController : This is just a simple setup to show how you can tweak a Null node and use it as a UI for parameters. 

